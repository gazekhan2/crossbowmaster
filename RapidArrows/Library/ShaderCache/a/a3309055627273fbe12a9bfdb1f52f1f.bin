<Q                         USE_SHAPE_LIGHT_TYPE_1      <  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _TimeParameters;
uniform 	vec4 _ProjectionParams;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM mediump vec4 unity_WorldTransformParams;
	UNITY_UNIFORM mediump vec4 unity_LightData;
	UNITY_UNIFORM mediump vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM mediump vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM mediump vec4 unity_SHAr;
	UNITY_UNIFORM mediump vec4 unity_SHAg;
	UNITY_UNIFORM mediump vec4 unity_SHAb;
	UNITY_UNIFORM mediump vec4 unity_SHBr;
	UNITY_UNIFORM mediump vec4 unity_SHBg;
	UNITY_UNIFORM mediump vec4 unity_SHBb;
	UNITY_UNIFORM mediump vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM vec4 RimColor;
	UNITY_UNIFORM vec2 Vector2_AB971143;
	UNITY_UNIFORM float Vector1_52D24AB4;
	UNITY_UNIFORM float Vector1_36BC0FD6;
	UNITY_UNIFORM float Vector1_3AEF5FB5;
	UNITY_UNIFORM float Vector1_7AA64B76;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
in highp vec3 in_POSITION0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_COLOR0;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD1;
out highp vec4 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
vec4 u_xlat2;
bool u_xlatb2;
vec4 u_xlat3;
bvec4 u_xlatb3;
vec4 u_xlat4;
bvec4 u_xlatb4;
vec2 u_xlat6;
vec2 u_xlat11;
bool u_xlatb11;
vec2 u_xlat12;
bool u_xlatb12;
vec2 u_xlat13;
float u_xlat15;
float u_xlat16;
bool u_xlatb16;
float u_xlat17;
bool u_xlatb17;
void main()
{
    u_xlat0.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat15 = _TimeParameters.x * Vector1_3AEF5FB5;
    u_xlat1.xy = vec2(u_xlat15) * Vector2_AB971143.xy + u_xlat0.xy;
    u_xlat1.xy = u_xlat1.xy * vec2(vec2(Vector1_52D24AB4, Vector1_52D24AB4));
    u_xlat11.xy = floor(u_xlat1.xy);
    u_xlat1.xy = fract(u_xlat1.xy);
    u_xlat2.xy = u_xlat11.xy + vec2(1.0, 1.0);
    u_xlat3 = u_xlat2.xyxy * vec4(289.0, 289.0, 289.0, 289.0);
    u_xlatb3 = greaterThanEqual(u_xlat3, (-u_xlat3.zwzw));
    u_xlat3.x = (u_xlatb3.x) ? float(289.0) : float(-289.0);
    u_xlat3.y = (u_xlatb3.y) ? float(289.0) : float(-289.0);
    u_xlat3.z = (u_xlatb3.z) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat3.w = (u_xlatb3.w) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat2.xy = u_xlat2.xy * u_xlat3.zw;
    u_xlat2.xy = fract(u_xlat2.xy);
    u_xlat2.xy = u_xlat2.xy * u_xlat3.xy;
    u_xlat15 = u_xlat2.x * 34.0 + 1.0;
    u_xlat15 = u_xlat2.x * u_xlat15;
    u_xlat2.x = u_xlat15 * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb2 = !!(u_xlat2.x>=(-u_xlat2.x));
#else
    u_xlatb2 = u_xlat2.x>=(-u_xlat2.x);
#endif
    u_xlat2.xz = (bool(u_xlatb2)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat15 = u_xlat15 * u_xlat2.z;
    u_xlat15 = fract(u_xlat15);
    u_xlat15 = u_xlat2.x * u_xlat15 + u_xlat2.y;
    u_xlat2.x = u_xlat15 * 34.0 + 1.0;
    u_xlat15 = u_xlat15 * u_xlat2.x;
    u_xlat2.x = u_xlat15 * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb2 = !!(u_xlat2.x>=(-u_xlat2.x));
#else
    u_xlatb2 = u_xlat2.x>=(-u_xlat2.x);
#endif
    u_xlat2.xy = (bool(u_xlatb2)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat15 = u_xlat15 * u_xlat2.y;
    u_xlat15 = fract(u_xlat15);
    u_xlat15 = u_xlat15 * u_xlat2.x;
    u_xlat15 = u_xlat15 * 0.024390243;
    u_xlat15 = fract(u_xlat15);
    u_xlat2.xy = vec2(u_xlat15) * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat15 = floor(u_xlat2.y);
    u_xlat3.x = (-u_xlat15) + u_xlat2.x;
    u_xlat3.y = abs(u_xlat2.x) + -0.5;
    u_xlat15 = dot(u_xlat3.xy, u_xlat3.xy);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat2.xy = vec2(u_xlat15) * u_xlat3.xy;
    u_xlat12.xy = u_xlat1.xy + vec2(-1.0, -1.0);
    u_xlat15 = dot(u_xlat2.xy, u_xlat12.xy);
    u_xlat2 = u_xlat11.xyxy + vec4(0.0, 1.0, 1.0, 0.0);
    u_xlat3 = u_xlat2 * vec4(289.0, 289.0, 289.0, 289.0);
    u_xlatb3 = greaterThanEqual(u_xlat3, (-u_xlat3));
    u_xlat4.x = (u_xlatb3.z) ? float(289.0) : float(-289.0);
    u_xlat4.y = (u_xlatb3.w) ? float(289.0) : float(-289.0);
    u_xlat4.z = (u_xlatb3.z) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat4.w = (u_xlatb3.w) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat3.x = (u_xlatb3.x) ? float(289.0) : float(-289.0);
    u_xlat3.y = (u_xlatb3.y) ? float(289.0) : float(-289.0);
    u_xlat3.z = (u_xlatb3.x) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat3.w = (u_xlatb3.y) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat2.zw = u_xlat2.zw * u_xlat4.zw;
    u_xlat2.xy = u_xlat2.xy * u_xlat3.zw;
    u_xlat2 = fract(u_xlat2);
    u_xlat2.xy = u_xlat2.xy * u_xlat3.xy;
    u_xlat12.xy = u_xlat2.zw * u_xlat4.xy;
    u_xlat3.x = u_xlat12.x * 34.0 + 1.0;
    u_xlat12.x = u_xlat12.x * u_xlat3.x;
    u_xlat3.x = u_xlat12.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb3.x = !!(u_xlat3.x>=(-u_xlat3.x));
#else
    u_xlatb3.x = u_xlat3.x>=(-u_xlat3.x);
#endif
    u_xlat3.xy = (u_xlatb3.x) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat12.x = u_xlat12.x * u_xlat3.y;
    u_xlat12.x = fract(u_xlat12.x);
    u_xlat12.x = u_xlat3.x * u_xlat12.x + u_xlat12.y;
    u_xlat17 = u_xlat12.x * 34.0 + 1.0;
    u_xlat12.x = u_xlat12.x * u_xlat17;
    u_xlat17 = u_xlat12.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb17 = !!(u_xlat17>=(-u_xlat17));
#else
    u_xlatb17 = u_xlat17>=(-u_xlat17);
#endif
    u_xlat3.xy = (bool(u_xlatb17)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat12.x = u_xlat12.x * u_xlat3.y;
    u_xlat12.x = fract(u_xlat12.x);
    u_xlat12.x = u_xlat12.x * u_xlat3.x;
    u_xlat12.x = u_xlat12.x * 0.024390243;
    u_xlat12.x = fract(u_xlat12.x);
    u_xlat12.xy = u_xlat12.xx * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat17 = floor(u_xlat12.y);
    u_xlat3.x = (-u_xlat17) + u_xlat12.x;
    u_xlat3.y = abs(u_xlat12.x) + -0.5;
    u_xlat12.x = dot(u_xlat3.xy, u_xlat3.xy);
    u_xlat12.x = inversesqrt(u_xlat12.x);
    u_xlat12.xy = u_xlat12.xx * u_xlat3.xy;
    u_xlat3 = u_xlat1.xyxy + vec4(-0.0, -1.0, -1.0, -0.0);
    u_xlat12.x = dot(u_xlat12.xy, u_xlat3.zw);
    u_xlat15 = u_xlat15 + (-u_xlat12.x);
    u_xlat13.xy = u_xlat1.xy * u_xlat1.xy;
    u_xlat13.xy = u_xlat1.xy * u_xlat13.xy;
    u_xlat4.xy = u_xlat1.xy * vec2(6.0, 6.0) + vec2(-15.0, -15.0);
    u_xlat4.xy = u_xlat1.xy * u_xlat4.xy + vec2(10.0, 10.0);
    u_xlat13.xy = u_xlat13.xy * u_xlat4.xy;
    u_xlat15 = u_xlat13.y * u_xlat15 + u_xlat12.x;
    u_xlat4 = u_xlat11.xyxy * vec4(289.0, 289.0, 289.0, 289.0);
    u_xlatb4 = greaterThanEqual(u_xlat4, (-u_xlat4.zwzw));
    u_xlat4.x = (u_xlatb4.x) ? float(289.0) : float(-289.0);
    u_xlat4.y = (u_xlatb4.y) ? float(289.0) : float(-289.0);
    u_xlat4.z = (u_xlatb4.z) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat4.w = (u_xlatb4.w) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat11.xy = u_xlat11.xy * u_xlat4.zw;
    u_xlat11.xy = fract(u_xlat11.xy);
    u_xlat11.xy = u_xlat11.xy * u_xlat4.xy;
    u_xlat12.x = u_xlat11.x * 34.0 + 1.0;
    u_xlat11.x = u_xlat11.x * u_xlat12.x;
    u_xlat12.x = u_xlat11.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb12 = !!(u_xlat12.x>=(-u_xlat12.x));
#else
    u_xlatb12 = u_xlat12.x>=(-u_xlat12.x);
#endif
    u_xlat12.xy = (bool(u_xlatb12)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat11.x = u_xlat11.x * u_xlat12.y;
    u_xlat11.x = fract(u_xlat11.x);
    u_xlat11.x = u_xlat12.x * u_xlat11.x + u_xlat11.y;
    u_xlat16 = u_xlat11.x * 34.0 + 1.0;
    u_xlat11.x = u_xlat11.x * u_xlat16;
    u_xlat16 = u_xlat11.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb16 = !!(u_xlat16>=(-u_xlat16));
#else
    u_xlatb16 = u_xlat16>=(-u_xlat16);
#endif
    u_xlat12.xy = (bool(u_xlatb16)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat11.x = u_xlat11.x * u_xlat12.y;
    u_xlat11.x = fract(u_xlat11.x);
    u_xlat11.x = u_xlat11.x * u_xlat12.x;
    u_xlat11.x = u_xlat11.x * 0.024390243;
    u_xlat11.x = fract(u_xlat11.x);
    u_xlat11.xy = u_xlat11.xx * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat16 = floor(u_xlat11.y);
    u_xlat4.x = (-u_xlat16) + u_xlat11.x;
    u_xlat4.y = abs(u_xlat11.x) + -0.5;
    u_xlat11.x = dot(u_xlat4.xy, u_xlat4.xy);
    u_xlat11.x = inversesqrt(u_xlat11.x);
    u_xlat11.xy = u_xlat11.xx * u_xlat4.xy;
    u_xlat1.x = dot(u_xlat11.xy, u_xlat1.xy);
    u_xlat6.x = u_xlat2.x * 34.0 + 1.0;
    u_xlat6.x = u_xlat2.x * u_xlat6.x;
    u_xlat11.x = u_xlat6.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb11 = !!(u_xlat11.x>=(-u_xlat11.x));
#else
    u_xlatb11 = u_xlat11.x>=(-u_xlat11.x);
#endif
    u_xlat11.xy = (bool(u_xlatb11)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat6.x = u_xlat11.y * u_xlat6.x;
    u_xlat6.x = fract(u_xlat6.x);
    u_xlat6.x = u_xlat11.x * u_xlat6.x + u_xlat2.y;
    u_xlat11.x = u_xlat6.x * 34.0 + 1.0;
    u_xlat6.x = u_xlat6.x * u_xlat11.x;
    u_xlat11.x = u_xlat6.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb11 = !!(u_xlat11.x>=(-u_xlat11.x));
#else
    u_xlatb11 = u_xlat11.x>=(-u_xlat11.x);
#endif
    u_xlat11.xy = (bool(u_xlatb11)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat6.x = u_xlat11.y * u_xlat6.x;
    u_xlat6.x = fract(u_xlat6.x);
    u_xlat6.x = u_xlat6.x * u_xlat11.x;
    u_xlat6.x = u_xlat6.x * 0.024390243;
    u_xlat6.x = fract(u_xlat6.x);
    u_xlat6.xy = u_xlat6.xx * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat11.x = floor(u_xlat6.y);
    u_xlat2.x = (-u_xlat11.x) + u_xlat6.x;
    u_xlat2.y = abs(u_xlat6.x) + -0.5;
    u_xlat6.x = dot(u_xlat2.xy, u_xlat2.xy);
    u_xlat6.x = inversesqrt(u_xlat6.x);
    u_xlat6.xy = u_xlat6.xx * u_xlat2.xy;
    u_xlat6.x = dot(u_xlat6.xy, u_xlat3.xy);
    u_xlat6.x = (-u_xlat1.x) + u_xlat6.x;
    u_xlat1.x = u_xlat13.y * u_xlat6.x + u_xlat1.x;
    u_xlat15 = u_xlat15 + (-u_xlat1.x);
    u_xlat15 = u_xlat13.x * u_xlat15 + u_xlat1.x;
    u_xlat15 = u_xlat15 * Vector1_36BC0FD6;
    u_xlat1.xy = vec2(u_xlat15) * Vector2_AB971143.xy;
    u_xlat15 = log2(abs(in_TEXCOORD0.y));
    u_xlat15 = u_xlat15 * Vector1_7AA64B76;
    u_xlat15 = exp2(u_xlat15);
    u_xlat15 = min(u_xlat15, 1.0);
    u_xlat1.xy = u_xlat1.xy * vec2(u_xlat15);
    u_xlat1.z = 0.0;
    u_xlat0.xyz = u_xlat0.xyz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_WorldToObject[1].xyz;
    u_xlat0.xyw = hlslcc_mtx4x4unity_WorldToObject[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * u_xlat0.zzz + u_xlat0.xyw;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToObject[3].xyz;
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyw = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * u_xlat0.zzz + u_xlat0.xyw;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_MatrixVP[3];
    gl_Position = u_xlat0;
    vs_TEXCOORD0 = in_TEXCOORD0;
    vs_TEXCOORD1 = in_COLOR0;
    u_xlat0.y = u_xlat0.y * _ProjectionParams.x;
    u_xlat1.xzw = u_xlat0.xwy * vec3(0.5, 0.5, 0.5);
    vs_TEXCOORD2.zw = u_xlat0.zw;
    vs_TEXCOORD2.xy = u_xlat1.zz + u_xlat1.xw;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec2 _ShapeLightBlendFactors1;
uniform 	vec4 _ShapeLightMaskFilter1;
uniform 	vec4 _ShapeLightInvertedFilter1;
uniform 	mediump float _HDREmulationScale;
uniform 	mediump float _UseSceneLighting;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM vec4 RimColor;
	UNITY_UNIFORM vec2 Vector2_AB971143;
	UNITY_UNIFORM float Vector1_52D24AB4;
	UNITY_UNIFORM float Vector1_36BC0FD6;
	UNITY_UNIFORM float Vector1_3AEF5FB5;
	UNITY_UNIFORM float Vector1_7AA64B76;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
UNITY_LOCATION(1) uniform mediump sampler2D _MaskTex;
UNITY_LOCATION(2) uniform mediump sampler2D _ShapeLightTexture1;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
in highp vec4 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_TARGET0;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
bool u_xlatb0;
vec4 u_xlat1;
mediump vec2 u_xlat16_1;
vec4 u_xlat2;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
vec3 u_xlat4;
void main()
{
    u_xlat0 = (-_ShapeLightInvertedFilter1) + vec4(1.0, 1.0, 1.0, 1.0);
    u_xlat16_1.xy = texture(_MaskTex, vs_TEXCOORD0.xy).xy;
    u_xlat16_2 = (-u_xlat16_1.x) + 1.0;
    u_xlat2 = vec4(u_xlat16_2) * _ShapeLightInvertedFilter1;
    u_xlat0 = u_xlat0 * u_xlat16_1.xxxx + u_xlat2;
    u_xlat0.x = dot(u_xlat0, _ShapeLightMaskFilter1);
    u_xlat4.xy = vs_TEXCOORD2.xy / vs_TEXCOORD2.ww;
    u_xlat4.xyz = texture(_ShapeLightTexture1, u_xlat4.xy).xyz;
    u_xlat1.xzw = u_xlat0.xxx * u_xlat4.xyz;
    u_xlat0.x = dot(_ShapeLightMaskFilter1, _ShapeLightMaskFilter1);
#ifdef UNITY_ADRENO_ES3
    u_xlatb0 = !!(u_xlat0.x!=0.0);
#else
    u_xlatb0 = u_xlat0.x!=0.0;
#endif
    u_xlat16_3.xyz = (bool(u_xlatb0)) ? u_xlat1.xzw : u_xlat4.xyz;
    u_xlat0.xyz = u_xlat16_3.xyz * _ShapeLightBlendFactors1.xxx;
    u_xlat1.xzw = u_xlat16_3.xyz * _ShapeLightBlendFactors1.yyy;
    u_xlat2 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat2.xyz = RimColor.xyz * u_xlat16_1.yyy + u_xlat2.xyz;
    u_xlat2 = u_xlat2 * vs_TEXCOORD1;
    u_xlat16_3.xyz = u_xlat2.xyz * u_xlat0.xyz + u_xlat1.xzw;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vec3(_HDREmulationScale);
    u_xlat16_0.xyz = u_xlat16_3.xyz * vec3(vec3(_UseSceneLighting, _UseSceneLighting, _UseSceneLighting));
    u_xlat16_3.x = (-_UseSceneLighting) + 1.0;
    u_xlat16_0.w = u_xlat2.w * _UseSceneLighting;
    SV_TARGET0 = u_xlat16_3.xxxx * u_xlat2 + u_xlat16_0;
    return;
}

#endif
                                $Globals8         _ShapeLightBlendFactors1                         _ShapeLightMaskFilter1                          _ShapeLightInvertedFilter1                           _HDREmulationScale                    0      _UseSceneLighting                     4          UnityPerMaterial(         RimColor                         Vector2_AB971143                        Vector1_52D24AB4                        Vector1_36BC0FD6                        Vector1_3AEF5FB5                         Vector1_7AA64B76                  $          $Globals`         _TimeParameters                          _ProjectionParams                           unity_MatrixVP                              UnityPerDraw�        unity_LODFade                     �      unity_WorldTransformParams                    �      unity_LightData                   �      unity_LightIndices                   �      unity_ProbesOcclusion                     �      unity_SpecCube0_HDR                   �      unity_LightmapST                  �      unity_DynamicLightmapST                      
   unity_SHAr                      
   unity_SHAg                       
   unity_SHAb                    0  
   unity_SHBr                    @  
   unity_SHBg                    P  
   unity_SHBb                    `  	   unity_SHC                     p     unity_ObjectToWorld                         unity_WorldToObject                  @             _MainTex                  _MaskTex                _ShapeLightTexture1                 UnityPerMaterial              UnityPerDraw          