﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup_FlameArrow : Powerup
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void ShowPowerup()
    {
        base.ShowPowerup();
        GetComponent<RectTransform>().localPosition = initialPosition;
        this.transform.parent.GetComponent<BoxCollider2D>().size = new Vector2(75, 30);
    }
}
