﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup_MultiArrow_Cross : Powerup
{
    [SerializeField]
    private List<Arrow> extraArrows = new List<Arrow>();
    public override void ShowPowerup()
    {
        base.ShowPowerup();

        extraArrows = MainGame.instance.AddNewArrows(2);
    }

    private void OnEnable()
    {
        Quiver.OnArrowFired += OnFireClicked;
    }

    private void OnDisable()
    {
        Quiver.OnArrowFired -= OnFireClicked;
    }

    private void OnFireClicked()
    {
        float degrees = 5;
        for (int i = 0; i < extraArrows.Count; i++)
        {
            extraArrows[i].transform.localRotation = Quaternion.Euler(new Vector3(0, 0, extraArrows[i].transform.localRotation.eulerAngles.z + degrees));
            degrees *= -1;
        }

        for (int i = 0; i < extraArrows.Count; i++)
        {
            extraArrows[i].Fire(MainGame.instance.CurrentLevel.arrow_Speed);
        }

        Hide();
    }

    public override void Hide()
    {
        for (int i = 0; i < extraArrows.Count; i++)
        {
            Destroy(extraArrows[i]);
        }
        base.Hide();
    }
}
