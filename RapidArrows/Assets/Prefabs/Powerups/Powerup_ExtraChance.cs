﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup_ExtraChance : Powerup
{
    [Header("DERIVED CLASS")]
    public GameObject plusOne;


    public override void ShowPowerup()
    {
        base.ShowPowerup();

        plusOne.SetActive(true);
    }

    public override void ActivatePowerup()
    {
        base.ActivatePowerup();
    }

    private void OnEnable()
    {
        Quiver.OnArrowFired += OnFireClicked;
    }

    private void OnDisable()
    {
        Quiver.OnArrowFired -= OnFireClicked;
    }

    private void OnFireClicked()
    {
        Yielder.instance.ExecuteAfterSeconds(1f, () =>
        {
            plusOne.SetActive(false);
            MainGame.instance.AddArrowsFromUsedOnes(1);
            Hide();
        });
    }

}
