﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [MenuItem("Custom Tools / Scene0 %#q")]
    static void OpenScene0()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/Scenes/Scene0.unity");
    }

    [MenuItem("Custom Tools / Home %#w")]
    static void OpenHomeScene()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/Scenes/Home.unity");
    }

    [MenuItem("Custom Tools / MainGame %#e")]
    static void OpenMainGameScene()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/Scenes/MainGame.unity");
    }
}
