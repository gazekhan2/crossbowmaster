﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Digit : MonoBehaviour {

	public Text[] digits = new Text[3];
	private Transform strip;

	void Start () {
		strip = transform.GetChild (0);	
	}

	public void SetDigitWithAnimation(char cChar, int direction, float fDuration, float delay){
		gameObject.SetActive (true);
		if(direction > 0){
			digits [2].text = cChar.ToString ();
		}else{
			digits [0].text = cChar.ToString ();
		}

		Yielder.instance.ExecuteAfterSeconds (delay, ()=>{
			strip.DOLocalMoveY (100 * direction, fDuration).SetEase(Ease.OutBounce).OnComplete (()=>{
				strip.localPosition = new Vector3(strip.localPosition.x, 0, strip.localPosition.z);
				digits [1].text = cChar.ToString ();
			});			
		});
	}

	public void SetDigit(char cCharArray){
		////for (int i = 0; i < cCharArray.Length; i++) {
			digits [1].text = cCharArray.ToString ();
		////}
	}
	
	#region color
	public void SetAlpha(float fAlpha){
		for (int i = 0; i < digits.Length; i++) {
			digits [i].color = new Color (digits [i].color.r, digits [i].color.g, digits [i].color.b, fAlpha);
		}
	}
	
	public void FadeAlpha(float fAlpha, float fDuration){
		for (int i = 0; i < digits.Length; i++) {
			digits [i].CrossFadeAlpha(fAlpha, fDuration, false);
		}
	}

	public void SetColor(Color color){
		for (int i = 0; i < digits.Length; i++) {
			digits [i].color = color;
		}
	}

	public void FadeColor(Color color, float fDuration){
		for (int i = 0; i < digits.Length; i++) {
			digits [i].CrossFadeColor(color, fDuration, false, false);
		}
			
	}
	#endregion
}
