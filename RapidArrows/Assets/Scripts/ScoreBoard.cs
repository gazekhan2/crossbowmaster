﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(Image))]
[RequireComponent(typeof(Mask))]
public class ScoreBoard : MonoBehaviour {

	public Digit _pfDigit;
	public int iOffset = 50;
	public int digitWidth = 50;
	public Transform gear;

	private List<Digit> dDigits = new List<Digit> ();
	private long lPrevNumber;
	private RectTransform mRectTransform;

	void Start () 
	{
		CreateDigits (10);
		mRectTransform = GetComponent<RectTransform> ();
	}

	public long number = 1234;
	int curDigitCount = 0;
	public void SetNumber (long iNumber, bool bAnimate = true)
	{
		number = iNumber;
		char[] digitArray = number.ToString ().ToCharArray ();
		System.Array.Reverse (digitArray);

		EnoughtNumberOfDigits (digitArray.Length);
		int direction = number > lPrevNumber ? -1 : 1;
		float fDuration = 0.25f;
		float delay = 0.1f;
		for (int i = 0; i < dDigits.Count; i++) {
			if(i < digitArray.Length){
				if(bAnimate){
					delay = (digitArray.Length - i) * 0.1f;
					dDigits [i].SetDigitWithAnimation (digitArray [i], direction, fDuration, delay);
				}else{
					dDigits [i].SetDigit (digitArray [i]);
				}
			}else{
				//dDigits [i].SetDigit (' ', direction, (digitArray.Length - i)*0.1f);
				dDigits [i].gameObject.SetActive (false);
			}
		}

		if(bAnimate)
			RotateGear (360f, fDuration + delay, 0.1f);

		mRectTransform.sizeDelta = new Vector2(iOffset + (digitWidth * digitArray.Length), mRectTransform.sizeDelta.y);

		curDigitCount = digitArray.Length;
		lPrevNumber = number;
	}

	void EnoughtNumberOfDigits(int noOfDigits)
	{
		if(noOfDigits > dDigits.Count){
			CreateDigits (noOfDigits-dDigits.Count);
		}	
	}

	void CreateDigits(int count){
		for (int i = 0; i < count; i++) {
			Digit digit = Instantiate (_pfDigit, this.transform) as Digit;
			float pos = ((digitWidth * i) + iOffset) * -1;
			digit.gameObject.transform.localPosition = new Vector3 (pos, digit.transform.localPosition.y, digit.transform.localPosition.z);
			digit.gameObject.SetActive (false);
			dDigits.Add (digit);
		}
	}

	void RotateGear(float fRotation, float fDuration, float fDelay){
		fRotation += gear.eulerAngles.z;
		Yielder.instance.ExecuteAfterSeconds (fDelay, ()=>{
			gear.DORotate (new Vector3 (0, 0, fRotation), fDuration, RotateMode.FastBeyond360).SetEase(Ease.OutBounce);
		});
	}

	string ToString(char[] array){
		string str = "";
		for (int i = 0; i < array.Length; i++) {
			str += array [i].ToString ();
		}
		return str;
	}

	public void SetAlpha(float fAlpha){
//		Debug.Log ("curDigitCount: " + curDigitCount);
		for (int i = 0; i < curDigitCount; i++) {			
			dDigits [i].SetAlpha (fAlpha);					
		}
	}

	public void FadeAlpha(float fAlpha, float fDuration){
//		Debug.Log ("curDigitCount: " + curDigitCount);
		for (int i = 0; i < curDigitCount; i++) {			
			dDigits [i].FadeAlpha (fAlpha, fDuration);					
		}
	}

	public void SetColor(Color color){
//		Debug.Log ("curDigitCount: " + curDigitCount);
		for (int i = 0; i < curDigitCount; i++) {			
			dDigits [i].SetColor (color);					
		}
	}
	
	public void FadeColor(Color color, float fDuration){
//		Debug.Log ("curDigitCount: " + curDigitCount);
		for (int i = 0; i < curDigitCount; i++) {			
			dDigits [i].FadeColor (color, fDuration);					
		}
	}
}
