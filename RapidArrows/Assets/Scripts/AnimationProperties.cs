﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class AnimationProperties : MonoBehaviour {

	[Header("Properties")]
	public bool doIBelongToALevel = true;
	public Transform mTransform;
	public List<AnimProps> animationType = new List<AnimProps>();
	public bool loop = false;
	public bool setInitialColorOnStart = false;
	public bool useColorFromGivenValues = false;
	public bool useRandomAnimationDuration = false;
	public float startDelay;
	public Vector2 animationDurationRange = new Vector2 ();

	[Header("Initial values")]
	public List<Vector3> listOfInitialPRSC = new List<Vector3> ();

	[Header("Final values")]
	public List<Vector3> listOfFinalPRSC = new List<Vector3> ();

	[Header("Tweeners")]
	public List<Tweener> listOfTweenersPRSC;// = new List<Tweener> ();

//	void Awake () {
//	}

	void Awake () {
		mTransform = transform;
		listOfTweenersPRSC = new List<Tweener> (animationType.Count);
//		for (int i = 0; i < animationType.Count; i++) {
//			listOfTweenersPRSC [i] = DOTween.Tween ();
//		}

		if(setInitialColorOnStart){
			
			if(!useColorFromGivenValues){

				int index1 = animationType.IndexOf(animationType.Find(x => x.animType == BGAnimType.color));
				int index = LevelManager.instance.GetStageIndexFromLevelID (GameDataManager.instance.currentLevelNumber);
				listOfInitialPRSC [index1] = new Vector3 
					(
						ColorPallette.instance.cameraBGColor [index].r,
						ColorPallette.instance.cameraBGColor [index].g,
						ColorPallette.instance.cameraBGColor [index].b
					);
				
				float r = (listOfInitialPRSC [index1].x + Random.Range ((10.0f / 255.0f), (25.0f / 255.0f)));
				float g = (listOfInitialPRSC [index1].y + Random.Range ((10.0f / 255.0f), (25.0f / 255.0f)));
				float b = (listOfInitialPRSC [index1].z + Random.Range ((10.0f / 255.0f), (25.0f / 255.0f)));
				listOfFinalPRSC [index1] = new Vector3 (r, g, b);

				if(mTransform.GetComponent<Image> () != null)
					mTransform.GetComponent<Image> ().color = new Color 
						(
							listOfInitialPRSC [index1].x, 
							listOfInitialPRSC [index1].y, 
							listOfInitialPRSC [index1].z, 
							1.0f
						);
			}
			
			startDelay = startDelay > 0 ? Random.Range ((startDelay - (startDelay / 2)), (startDelay + (startDelay / 2))) : startDelay;
		}

	}

	public float GetAnimationDuration () {
		if(useRandomAnimationDuration){
			return (float)Random.Range (animationDurationRange.x, animationDurationRange.y);
		}
		else{
			return (float)animationDurationRange.x;
		}
	}

}

public enum BGAnimType {
	select = -1,
	move = 0,
	rotate = 1,
	scale = 2,
	color = 3
}

[System.Serializable]
public class AnimProps
{
	public BGAnimType animType = BGAnimType.rotate;
	public Ease easeType = Ease.Linear;
	public LoopType loopType = LoopType.Incremental;
}
