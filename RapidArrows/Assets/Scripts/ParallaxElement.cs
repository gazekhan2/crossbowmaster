﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxElement : MonoBehaviour, IComparable
{
    // Start is called before the first frame update
    public int mLayer = 0;

    private RectTransform mRectTransform;
    private Vector2 initialPosition;
    void Awake()
    {
        mRectTransform = GetComponent<RectTransform>();
        initialPosition = mRectTransform.anchoredPosition;
    }

    public void UpdateTransform(Vector2 offset)
    {
        mRectTransform.anchoredPosition = initialPosition + (offset * mLayer);
    }

    public int CompareTo(object parallaxElement)
    {
        return this.mLayer.CompareTo((parallaxElement as ParallaxElement).mLayer);
    }
}
