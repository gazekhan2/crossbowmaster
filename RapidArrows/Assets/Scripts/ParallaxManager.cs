﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxManager : MonoBehaviour
{
    public static ParallaxManager instance;

    public float multiplier = 10f;
    public Vector2 axes = new Vector2(0, 0);

    private ParallaxElement[] elements = new ParallaxElement[] { };
    private bool gyroEnabled = false;
    private Gyroscope gyro;

    private void Awake()
    {
        instance = this;
        elements = FindObjectsOfType<ParallaxElement>();
        Array.Sort(elements);
        gyroEnabled = EnableGyro();
        //for (int i = 0; i < elements.Length; i++)
        //{
        //    Debug.Log("elements[" + i + "]: " + elements[i].mLayer);
        //}
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    float offsetX = 0f;
    float offsetY = 0f;
    void FixedUpdate()
    {
        if (gyroEnabled)
        {
            offsetX = gyro.attitude.x;
            offsetX = Mathf.Clamp(offsetX, -1 * multiplier, multiplier);
            offsetY = gyro.attitude.y;
            offsetY = Mathf.Clamp(offsetY, -1 * multiplier, multiplier);
            for (int i = 0; i < elements.Length; i++)
            {
                elements[i].UpdateTransform(new Vector2(offsetX, offsetY) * multiplier);                
            }
        }
#if UNITY_EDITOR
        for (int i = 0; i < elements.Length; i++)
        {
            elements[i].UpdateTransform(axes * multiplier);
        }
#endif
    }

    bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;

            return true;
        }

        return false;
    }
}
