﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BackgroundSetup : MonoBehaviour {

	public List<AnimationProperties> animatingUnits = new List<AnimationProperties> ();

	private float animationDuration = 0;
	private Tween [] tweens = new Tween [4];

	void Start () {
		//StartBGAnimation ();
	}

	public void StartBGAnimation (float startDelay = 0.0f) {
		for (int i = 0; i < animatingUnits.Count; i++) {
			for (int j = 0; j < animatingUnits[i].animationType.Count; j++) {
				
				BGAnimType animationType = animatingUnits [i].animationType [j].animType;
				animationDuration = animatingUnits [i].GetAnimationDuration(); //(float)Random.Range (animatingUnits [i].animationDurationRange.x, animatingUnits [i].animationDurationRange.y);
				startDelay += animatingUnits [i].startDelay;

				switch(animationType){

				case BGAnimType.move:
					Move (animatingUnits [i], startDelay);
						break;
				case BGAnimType.rotate:
					Rotate (animatingUnits [i], startDelay);
						break;
				case BGAnimType.scale:
					Scale (animatingUnits [i], startDelay);
						break;
				case BGAnimType.color:
					Color (animatingUnits [i], startDelay);
						break;
				}
			}
		}
	}

	void Move (AnimationProperties animatingUnit, float startDelay) {
		Yielder.instance.ExecuteAfterSeconds (startDelay, ()=>{
			int index = animatingUnit.animationType.IndexOf(animatingUnit.animationType.Find(x => x.animType == BGAnimType.move));
			animatingUnit.mTransform.localPosition = animatingUnit.listOfInitialPRSC [index];
			tweens[0] = animatingUnit.mTransform.DOLocalMove (animatingUnit.listOfFinalPRSC [index], animationDuration).SetLoops(-1, animatingUnit.animationType[index].loopType).SetEase(animatingUnit.animationType[index].easeType);
		});
	}

	void Rotate (AnimationProperties animatingUnit, float startDelay) {
		Yielder.instance.ExecuteAfterSeconds (startDelay, ()=>{
			int index = animatingUnit.animationType.IndexOf(animatingUnit.animationType.Find(x => x.animType == BGAnimType.rotate));
			animatingUnit.mTransform.rotation = Quaternion.Euler(animatingUnit.listOfInitialPRSC [index]);
			tweens[1] = animatingUnit.mTransform.DOLocalRotate(animatingUnit.listOfFinalPRSC [index], animationDuration).SetLoops(-1, animatingUnit.animationType[index].loopType).SetEase(animatingUnit.animationType[index].easeType);
		});
	}

	void Scale (AnimationProperties animatingUnit, float startDelay) {
		Yielder.instance.ExecuteAfterSeconds (startDelay, ()=>{
//			tweens[2] = 
		});
	}

	void Color (AnimationProperties animatingUnit, float startDelay) {
		int index = animatingUnit.animationType.IndexOf(animatingUnit.animationType.Find(x => x.animType == BGAnimType.color));
		Vector3 rgb = animatingUnit.listOfInitialPRSC [index];
		Color initialColor = new Color (rgb.x, rgb.y, rgb.z, 1.0f);
		rgb = animatingUnit.listOfFinalPRSC [index];
		Color finalColor = new Color (rgb.x, rgb.y, rgb.z, 1.0f);

		Yielder.instance.ExecuteAfterSeconds (startDelay, ()=>{
			Image image = animatingUnit.mTransform.GetComponent<Image> ();
			image.color = initialColor;
			tweens[3] = image.DOColor (finalColor, animationDuration).SetLoops (-1, animatingUnit.animationType[index].loopType).SetEase(animatingUnit.animationType[index].easeType);
		});
	}

	public void Pause ()
	{
		for (int i = 0; i < tweens.Length; i++) {
			if(tweens[i] != null){
				tweens [i].Pause ();
			}
		}
	}

	public void UnPause ()
	{
		for (int i = 0; i < tweens.Length; i++) {
			if(tweens[i] != null){
				tweens [i].Play ();
			}
		}
	}
}
