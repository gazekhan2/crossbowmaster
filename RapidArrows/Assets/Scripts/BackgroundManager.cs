﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundManager : MonoBehaviour {

	public static BackgroundManager instance;

	private List<BackgroundSetup> backgroundSetups = new List<BackgroundSetup>();

	void Awake () {
		instance = this;
	}

	public void SetupLevelBackground () {
		backgroundSetups.Clear ();
		Transform background = GameObject.Find ("Backgrounds").transform;
		for (int i = 0; i < background.childCount; i++) {
			backgroundSetups.Add (background.GetChild (i).GetComponent<BackgroundSetup>());
		}

		int index = Random.Range (0, backgroundSetups.Count);
		backgroundSetups [index].gameObject.SetActive (true);
		backgroundSetups [index].StartBGAnimation (0.0f);
	}
}
	
