﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour
{
    [Header("BASE CLASS")]
    public int _ID = -1;
    public bool bAttachtoArrow = false;
    public Vector3 initialPosition = Vector3.zero;
    public Vector3 initialRotation = Vector3.zero;
    public Vector3 initialScale = Vector3.one;

    private RectTransform mRectTransform;

    private void Awake()
    {
        
    }
    void Start()
    {
        
    }

    public virtual void ShowPowerup()
    {

    }

    public virtual void ActivatePowerup()
    {

    }

    public virtual void Init(Transform aParent)
    {
        this.transform.SetParent(aParent);

        mRectTransform = GetComponent<RectTransform>();
        mRectTransform.localPosition = initialPosition;
        mRectTransform.localRotation = Quaternion.Euler(initialRotation);
        mRectTransform.localScale = initialScale;
    }

    public virtual void Hide()
    {
        this.transform.SetParent(PowerupManager.instance.transform);
        this.gameObject.SetActive(false);
    }
}
