﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PowerupManager : MonoBehaviour
{
    public static PowerupManager instance;

    public List<Powerup> powerups = new List<Powerup>();

    private List<Powerup> powerUpPool = new List<Powerup>();
    private Powerup currentPowerup = null;
    private void Awake()
    {
        instance = this;
    }

    public void SelectPowerup(int aPowerupID)
    {
        if(currentPowerup != null)
        {
            currentPowerup.Hide();
        }

        Powerup tPup = GetPowerup(aPowerupID);
        currentPowerup = tPup;

        Transform powerupParent = currentPowerup.bAttachtoArrow ? MainGame.instance.CurrentLevel.GetCurrentArrows()[0].transform : MainGame.instance.CurrentLevel.transform;
        currentPowerup.Init(powerupParent);

        ShowCurrentPowerup();
    }
    public Powerup ShowCurrentPowerup()
    {
        if (currentPowerup == null)
            return null;

        switch (currentPowerup._ID)
        {
            case GameKeys.POWERUP_EXTRA_CHANCE:
                (currentPowerup as Powerup_ExtraChance).ShowPowerup();
                break;
            case GameKeys.POWERUP_FLAME_ARROW:
                (currentPowerup as Powerup_FlameArrow).ShowPowerup();
                break;
            case GameKeys.POWERUP_MULTI_ARROW_CROSS:
                (currentPowerup as Powerup_MultiArrow_Cross).ShowPowerup();
                break;
        }
        
        return currentPowerup;
    }

    Powerup GetPowerup(int aPowerupID)
    {
        Powerup tpup = powerUpPool.Find(x => x._ID == aPowerupID);
        if(tpup == null)
        {
            tpup = powerups.Find(x => x._ID == aPowerupID);
            tpup = Instantiate(tpup) as Powerup;
            //tpup.Hide();
        }

        return tpup;
    }

}
