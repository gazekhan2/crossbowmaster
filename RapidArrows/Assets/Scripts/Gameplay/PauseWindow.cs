﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PauseWindow : MonoBehaviour {

	public static PauseWindow instance;

	public Transform pauseWindow;
	public Image pauseBG;
	public Image button_Home;
	public Image button_Resume;
	public Text text_Home;
	public Text text_Resume;
	public Image[] buttonBGs;

	void Awake () {
		instance = this;
	}

	void Start () {
		Show (false);
	}

//	public void Init () 
//	{
//		text_Home.color = ColorPallette.instance.bulbInActiveColor [GameDataManager.instance.currentLevelNumber];
//		text_Resume.color = ColorPallette.instance.bulbInActiveColor [GameDataManager.instance.currentLevelNumber];
//	}

	public void Show (bool show) {
		if(show){
			Show ();
		}
		else{
			Hide ();
		}
	}

	private void Show () {
		pauseWindow.gameObject.SetActive (true);
		for (int i = 0; i < buttonBGs.Length; i++) {
			buttonBGs[i].DOFade (1f, 0.25f);
//			buttonBGs[i].transform.localScale = new Vector3(1f, 0.4f, 1f);
			Yielder.instance.DoScale (buttonBGs[i].transform, new Vector3(3f, 0.4f, 1f), 0.25f, 0.25f, Ease.OutExpo);
		}
		pauseBG.DOFade (70.0f / 255.0f, 0.25f);
		Yielder.instance.ExecuteAfterSeconds (0.5f, ()=>{
			text_Home.DOFade (1.0f, 0.25f);
			text_Resume.DOFade (1.0f, 0.25f);
		});
	}

	private void Hide () {
		pauseWindow.gameObject.SetActive (false);
		for (int i = 0; i < buttonBGs.Length; i++) {
			buttonBGs[i].DOFade (0f, 0.0f);
			buttonBGs[i].transform.localScale = new Vector3(0f, 0.4f, 1f);
		}
		pauseBG.DOFade (0.0f, 0.0f);
		text_Home.DOFade (0.0f, 0.0f);
		text_Resume.DOFade (0.0f, 0.0f);
	}

}
