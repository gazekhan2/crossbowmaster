﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class RemainingArrowsItem : MonoBehaviour {

	public Transform _BG;
	public Text tRemainingArrowsText;

	private float iBGInitialPosX;

	void Awake () {
		iBGInitialPosX = _BG.localPosition.x;
	}

	void Start () {
		
	}

	public void Show (string text, float fAnimDuration, float fDelay) {
		
		SetText (text);
		tRemainingArrowsText.color = ColorPallette.instance.bulbInActiveColor [LevelManager.instance.GetStageIndexFromLevelID (GameDataManager.instance.currentLevelNumber)];
		Yielder.instance.ExecuteAfterSeconds (fDelay, (() => {
			_BG.DOLocalMoveX (0, fAnimDuration).SetEase (Ease.OutBounce).OnComplete (() => {
				
			});			
		}));
	}

	public void Hide (float fDelay) {
		Yielder.instance.ExecuteAfterSeconds (fDelay, (() => {
			_BG.DOLocalMoveX (iBGInitialPosX, 0.4f).SetEase (Ease.InBounce).OnComplete (() => {

			});			
		}));
	}

	public void SetText (string text) {
		tRemainingArrowsText.text = text;
	}

}
