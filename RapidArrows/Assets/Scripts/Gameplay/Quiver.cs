﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Quiver : MonoBehaviour {

	public Tweener quiverTweener = null;
	public int iNumberOfArrowsNeeded = 0;
	public List<Arrow> listOfRemainingArrows = new List<Arrow>();
	//public Text remainingArrows_Text;
	public bool outOfArrows = false;

	[Header("Movement Properties")]
	public bool doesQuiverRotate = true;	
	public int mDirection = -1;
	public float mDuration = 4.0f;
	public LoopType mLoopType = LoopType.Incremental;
	public Ease mEase = Ease.Linear;
	public float mFinalAngle = 360.0f;
	public RotateMode mRotateMode = RotateMode.FastBeyond360;
	public int iQuiverIndex;

	private Transform mTransform;
	private List<Arrow> listOfUsedArrows = new List<Arrow>();

	public static UnityAction OnArrowFired;

	void Awake()
	{
		
		//remainingArrows_Text = transform.parent.Find("RemainingArrows").GetComponent<Text>();
		
	}
	void Start () {
		mTransform = transform;
		Init ();
		MainGame.instance.CurrentLevel.RegisterQuiver(this);
		//UpdateText ();
	}

	void Init () {
		for (int i = 0; i < iNumberOfArrowsNeeded; i++) {
			Arrow arrow = Instantiate (GameDataManager.instance._pfArrow, mTransform) as Arrow;
			listOfRemainingArrows.Add (arrow);
		}
	}

	public void RotateQuiver(/*int direction, float duration, LoopType loopType = LoopType.Incremental, 
							 Ease ease = Ease.Linear, float finalAngle = 360.0f*/) {
		if (!doesQuiverRotate)
			return;
		quiverTweener.Kill ();
		quiverTweener = null;
		quiverTweener = 
			mTransform.DORotate (new Vector3 (0, 0, mDirection * mFinalAngle), mDuration, mRotateMode)
			.SetLoops (-1, mLoopType).SetEase (mEase).OnComplete (()=>{
			});
	}

	public void ResetQuiver(GameObject quiver) {
		quiverTweener.Kill();
		quiverTweener = null;
		quiver.transform.rotation = Quaternion.Euler (Vector3.zero);
	}

	public void Fire (float speed) {
		if (!this.gameObject.activeSelf)
			return;
		
		if(!outOfArrows){
			Arrow arrowToUse = listOfRemainingArrows[0];
			listOfRemainingArrows.RemoveAt (0);

			if(listOfRemainingArrows.Count <= 0)
			outOfArrows = true;
			
			arrowToUse.Fire(speed);
			listOfUsedArrows.Add (arrowToUse);
			UpdateText ();

			OnArrowFired?.Invoke();
		}
	}

	public void AddArrowsFromUsedOnes (int numberOfArrowsToAdd) {
		
		if(listOfUsedArrows.Count >= numberOfArrowsToAdd) {	
			List<Arrow> toRemoveList = new List<Arrow> ();
			for (int i = 0; i < numberOfArrowsToAdd; i++) {
				listOfUsedArrows [i].Reset ();
				toRemoveList.Add (listOfUsedArrows [i]);
			}

			for (int i = 0; i < toRemoveList.Count; i++) {
				listOfUsedArrows.Remove (toRemoveList[i]);
				listOfRemainingArrows.Add (toRemoveList [i]);
			}
			
			UpdateText ();
			outOfArrows = false;

		}else{
			Debug.LogError ("Number of arrows to add is more than the number of available arrows");
		}
	}

	public Arrow[] AddNewArrows(int numberOfArrowsToAdd, bool makeAPartofLevel = false)
	{
		Arrow[] newArrows = new Arrow[numberOfArrowsToAdd];
		for (int i = 0; i < numberOfArrowsToAdd; i++)
		{
			Arrow arrow = Instantiate(GameDataManager.instance._pfArrow, mTransform) as Arrow;
			newArrows[i] = arrow;
			if (makeAPartofLevel)
			{
				listOfRemainingArrows.Add(arrow);
				UpdateText();
			}
		}

		return newArrows;
	}

	private void UpdateText () {
		//remainingArrows_Text.text = listOfRemainingArrows.Count.ToString ();
		RemainingArrows.instance.SetRemainingArrows (iQuiverIndex, listOfRemainingArrows.Count.ToString ());
	}

	public void Pause () {
		quiverTweener.Pause ();
	}

	public void UnPause () {
		quiverTweener.Play ();
	}

	public Arrow GetCurrentArrow()
	{
		return listOfRemainingArrows[0];
	}
}
