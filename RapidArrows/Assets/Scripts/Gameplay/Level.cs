﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using DG.Tweening;

public class Level : MonoBehaviour {

	public int levelNumber = 0;
	public int currentWave = 0;
	public List<Transform> listOfWaves = new List<Transform>();
	public List<Bulb> listOfBulbs = new List<Bulb> ();
	public List<Quiver> listOfQuivers = new List<Quiver>();
	public List<BackgroundSetup> listAnimatingUnits = new List<BackgroundSetup>();
	public int iTotalArrows = 0;
	public int iRemainingArrows = 0;
	public long levelWinAmount = 0;
	public float arrow_Speed = 40.0f;

	void Awake () {
		MainGame.instance.CurrentLevel = this;
	}
	void Start () {
		
	}

	void OnEnable () {
		MainGame.instance.PauseGameEvent += PauseGame;
		MainGame.instance.UnPauseGameEvent += UnPauseGame;
		MainGame.instance.FireArrowEvent += Fire;
	}

	void OnDisable () {
		MainGame.instance.PauseGameEvent -= PauseGame;
		MainGame.instance.UnPauseGameEvent -= UnPauseGame;
		MainGame.instance.FireArrowEvent -= Fire;
	}

	public void InitLevel (int levelNum) {
		levelNumber = levelNum;
		currentWave = 0;
	}
	public void ShowLevel () {
		ShowNextStage ();
	}
	public void StageComplete () {
		ShowNextStage ();
	}

	public void ShowNextStage () {
		listOfWaves [currentWave].gameObject.SetActive (true);
		foreach (Transform child in listOfWaves[currentWave]) {
		}
		Show (true);
		currentWave++;
	}

	public void RegisterBulb (Bulb newBulb) {
		listOfBulbs.Add (newBulb);
	}

	public void Show (bool animate) {
		if(animate){
			float delay = 0.25f;
			float duration = 0.5f;
			float totalAnimationTime = duration;

			for (int i = 0; i < listOfBulbs.Count; i++) {
				listOfBulbs [i].InitBulb (levelNumber);
				listOfBulbs [i].Show (delay * i, duration);
				totalAnimationTime += delay;
			}

//			totalAnimationTime += RemainingArrows.instance.Init ();

			Yielder.instance.ExecuteAfterSeconds (totalAnimationTime, ()=>{
				RemainingArrows.instance.Init ();
				MainGame.instance.StartGame ();
			});
		}
		else{

		}
	}

	public void CheckForGameComplete () {
		List<Bulb> nonBrokenBulbs = listOfBulbs.FindAll(x => x.isBroken == false);
		if(nonBrokenBulbs.Count <= 0){
//			MainGame.instance.message.text = "YOU WON!!!";
			print ("Game Complete");
			MainGame.instance.GameOver (true);
		}
		else{
			OutOfArrows ();
		}
	}

	public void Play() {
		for (int i = 0; i < listOfQuivers.Count; i++) {
			listOfQuivers[i].RotateQuiver(/*-1, 4.0f*/);
		}
		StartAnimatingUnits ();
	}

	public void Fire () {

		for (int i = 0; i < listOfQuivers.Count; i++) {
			listOfQuivers[i].Fire(arrow_Speed);
		}
	}

	public void OutOfArrows (){
		List<Quiver> qWithArrows = listOfQuivers.FindAll(x => x.outOfArrows == false);

		if(qWithArrows.Count <= 0){
			print ("OutOfArrows");
//			MainGame.instance.message.text = "OUT OF ARROWS!";
			MainGame.instance.GameOver(false);
		}
	}

	public void AddArrowsFromUsedOnes (int numberOfArrowsToAdd) {
		for (int i = 0; i < listOfQuivers.Count; i++) {
			listOfQuivers [i].AddArrowsFromUsedOnes (numberOfArrowsToAdd);
		}
	}

	public List<Arrow> AddNewArrows(int numberOfArrowsToAdd, bool makeAPartofLevel = false)
	{
		List<Arrow> newArrows = new List<Arrow>();
		for (int i = 0; i < listOfQuivers.Count; i++)
		{
			Arrow[] tArrows = new Arrow[numberOfArrowsToAdd];
			tArrows = listOfQuivers[i].AddNewArrows(numberOfArrowsToAdd, makeAPartofLevel);
			for (int j = 0; j < tArrows.Length; j++)
			{
				newArrows.Add(tArrows[j]);
			}
		}

		return newArrows;
	}

	void PauseGame () {
		print("PauseGame called");
		for (int i = 0; i < listOfQuivers.Count; i++) {
			listOfQuivers [i].Pause ();
		}

		for (int i = 0; i < listAnimatingUnits.Count; i++) {
			listAnimatingUnits[i].Pause ();
		}
	}

	void UnPauseGame () {
		print("UnPauseGame called");
		for (int i = 0; i < listOfQuivers.Count; i++) {
			listOfQuivers [i].UnPause ();
		}

		for (int i = 0; i < listAnimatingUnits.Count; i++) {
			listAnimatingUnits[i].UnPause ();
		}
	}

	public void RegisterQuiver (Quiver quiver){
		quiver.iQuiverIndex = listOfQuivers.Count;

		listOfQuivers.Add(quiver);
		iTotalArrows += quiver.listOfRemainingArrows.Count;
		iRemainingArrows = iTotalArrows;
	}

	public void StartAnimatingUnits () {
		for (int i = 0; i < listAnimatingUnits.Count; i++) {
			listAnimatingUnits[i].StartBGAnimation (0.0f);
		}
	}

	public Arrow[] GetCurrentArrows()
	{
		Arrow[] tArray = new Arrow[listOfQuivers.Count];
		for (int i = 0; i < listOfQuivers.Count; i++)
		{
			tArray[i] = listOfQuivers[i].GetCurrentArrow();
		}

		return tArray;
	}
}
