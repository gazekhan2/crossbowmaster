﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Arrow : MonoBehaviour {

//	public Transform startPoint;
//	public Transform endPoint;

	private RaycastHit2D hit;
	private Transform mTransform;
	private Vector3 initialPosition;
	private Vector3 initialRotation;
	private Vector3 initialScale;
	private Transform initialParent;

	void Start () {
		mTransform = transform;
		initialPosition = mTransform.localPosition;
		initialRotation = mTransform.localRotation.eulerAngles;
		initialScale = mTransform.localScale;
		initialParent = mTransform.parent;
	}

	public void Fire (float speed, bool isInGameArrow = true) {
//		Debug.Break ();
		Vector2 direction = mTransform.right;//this.endPoint.position - this.startPoint.position;
		hit = Physics2D.Raycast (mTransform.position, direction, 10000);
//		Debug.LogError ("direction: " + direction.ToString());
		mTransform.SetParent(GameDataManager.instance.usedArrowParent);

		mTransform.DOMove ((direction * 20), (isInGameArrow ? speed : speed/2.0f)).SetSpeedBased ().SetEase (Ease.Linear);
////		if(hit.collider != null){
//////			mTransform.DOMove ((hit.point * (isInGameArrow ? 5 : 1)), speed).SetSpeedBased ().SetEase (Ease.Linear);

////			if(HomeScreenManager.instance != null){
////				HomeScreenManager.instance.ArrowHitSomething (hit.collider);
////			}

////			if(MainGame.instance != null){
////				MainGame.instance.ArrowHitSomething (hit.collider);
////			}
////		}
////		else{
////			if(HomeScreenManager.instance != null){
////				HomeScreenManager.instance.ArrowHitNothing ();
////			}

////			if(MainGame.instance != null){
////				MainGame.instance.ArrowHitNothing ();
////			}

////		}
	}

	public void Reset () {
		Debug.Log ("Resetting: " + mTransform.name + " :: " + initialParent.name);
		mTransform.SetParent (initialParent);
		mTransform.localPosition = initialPosition;
		mTransform.localRotation = Quaternion.Euler(initialRotation);
		mTransform.localScale = initialScale;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (MainGame.instance != null)
		{
			MainGame.instance.ArrowHitSomething(other);
		}
		else if (HomeScreenManager.instance != null)
		{
			HomeScreenManager.instance.ArrowHitSomething(other);
		}
	}

	//Powerup powerup;
	//public Powerup SetPowerup(Powerup aPowerup)
	//{
	//	powerup = aPowerup;
	//	powerup.Init(this.transform);
	//	powerup.ShowPowerup();

	//	return powerup;
	//}
}
