﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameKeys
{
    public const int POWERUP_EXTRA_CHANCE           = 1;
    public const int POWERUP_FLAME_ARROW            = 2;
    public const int POWERUP_MULTI_ARROW_CROSS      = 3;

}
