﻿using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using DG.Tweening;

public class LevelManager : MonoBehaviour {

	public static LevelManager instance;

	public int levelToLoad = -1;
	public Transform levelParent;

	void Awake () {
		instance = this;
	}

	void Start () {
		
	}

	public void LoadLevel (int levelNumber){
		levelNumber = GameDataManager.instance.usingCheats ? GameDataManager.instance.cheatLevelNumber : Random.Range(1,5); //TODO: remove this
		Debug.Log ("levelNumber: " + levelNumber);
		GameObject levelGO = (GameObject)Resources.Load("Levels/Level" + levelNumber, typeof(GameObject));
		levelGO = Instantiate (levelGO, levelGO.transform.position, levelGO.transform.rotation) as GameObject;
		levelGO.transform.SetParent (levelParent);
		levelGO.transform.localPosition = levelGO.transform.position;
		levelGO.GetComponent<Level> ().InitLevel (levelNumber);
		levelGO.GetComponent<Level> ().ShowLevel ();
		levelGO.GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;
		levelGO.GetComponent<RectTransform> ().localScale = Vector3.one;
	}

	public void StageChange (int index) {

	}

	public int GetLevelUnlockStatus (int levelNumber) {
		return PlayerPrefs.GetInt ("level_" + levelNumber, 0);
	}

	public void UnlockLevel (int levelNumber) {
		PlayerPrefs.SetInt ("level_" + levelNumber, 1);
	}

	public int GetStageIndexFromLevelID (int levelID) {
		//print ("levelID: " + levelID);
		levelID--;
		int MOD = (int)(levelID % 10);
		int sub = levelID - MOD;
		int index = (int)(sub / 10);
//		print ("levelID: " + levelID + " :: MOD: " + MOD + " :: sub: " + sub + " :: index: " + index);
		return index;
	}

	public void LoadScene (string sceneName, int levelNumber = 0) {
		DOTween.KillAll ();
		GameDataManager.instance.currentLevelNumber = levelNumber > 0 ? levelNumber : -1;
		SceneManager.LoadScene (sceneName);
	}
}
