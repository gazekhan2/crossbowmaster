﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CurrencyManager : MonoBehaviour {

	public static CurrencyManager instance;
	public static CurrencyType coins = CurrencyType.primary;
	public static CurrencyType gems = CurrencyType.secondary;

	public Dictionary<CurrencyType, int> currency = new Dictionary<CurrencyType, int>();

	void Awake () {
		instance = this;
	}

	void Start () {
		InitializeCurrency (GetCurrencyFromPrefs(coins), GetCurrencyFromPrefs(gems));
		PlayerPrefs.DeleteAll ();
	}

	public void InitializeCurrency (int coins, int gems) {
		currency.Add (CurrencyType.primary, coins);
		currency.Add (CurrencyType.secondary, gems);
	}

	public void AwardCurrency (int amount, CurrencyType type) {
		
	}

	public int AddCurrency (int amount, CurrencyType type) {
		int afterAdd = amount + currency[type];
		return afterAdd;
	}

	public int DeductCurrency (int amount, CurrencyType type) {
		int afterReduce = (currency[type] - amount);
		afterReduce = afterReduce < 0 ? 0 : afterReduce;
		return afterReduce;
	}

	public int SetCurrencyToPrefs (CurrencyType type, int amount) {
		return PlayerPrefs.GetInt ("currency_"+type.ToString(), amount);
	}

	public int GetCurrencyFromPrefs(CurrencyType type){
		return PlayerPrefs.GetInt ("currency_"+type.ToString());
	}
}

public enum CurrencyType {
	primary,
	secondary
}
