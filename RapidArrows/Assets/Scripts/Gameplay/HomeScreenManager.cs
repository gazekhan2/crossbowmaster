﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class HomeScreenManager : MonoBehaviour {

	public static HomeScreenManager instance;

	public Transform[] startButtons = new Transform[2];
	public Image bulb;
	public Text [] titleTexts = new Text[2];
	public HomeScreenGearRotate[] gears = new HomeScreenGearRotate[2];
	public HomeScreenGearRotate levelSelectGear;
	public GameObject lightParticle;
	public GameObject startText;
	public Image startBorder;
	public RectTransform bigArrow;
	public GameObject particle_Blast;
	public Camera mainCamera;
	public int numberOfStages = 5;
	public ScrollSnapper stageScrollView;
	//public Text stageText;
	public Transform stagesParent;
	public Image shield;
	public List<LevelSelectionBulb> stages = new List<LevelSelectionBulb> ();
	public List<Arrow> arrows = new List<Arrow> ();


	[Header("Cheat Variables")]
	public InputField cInput_LevelNumber;
	public Dropdown cInput_StageNumberIndex;


	private Transform mTransform;
	private int arrowIndex = 0;


	public void SetCheats () {
		GameDataManager.instance.SetCheats (int.Parse(cInput_LevelNumber.text), cInput_StageNumberIndex.value);
		LevelManager.instance.LoadScene ("MainGame", GameDataManager.instance.cheatStageNumberIndex);
	}

	void Awake () {
		PlayerPrefs.DeleteAll ();
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 10; j++) {
				int levelNumber = (i * 10) + j + 1;
				PlayerPrefs.SetInt ("level_" + levelNumber, Random.Range(0,2));
			}
		}
		instance = this;
		mTransform = this.transform;
		GameDataManager.instance.usedArrowParent = GameObject.Find ("UsedArrowParent").transform;
	}

	void OnEnable () {
		stageScrollView.SnappingEvent += OnStageSwipe;
	}

	void OnDisable () {
		stageScrollView.SnappingEvent -= OnStageSwipe;
	}

	void Start () {
		ResetLight ();

		Yielder.instance.ExecuteAfterSeconds (0.5f, ()=>{
			InitStages ();
			for (int i = 0; i < gears.Length; i++) {
				gears [i].Rotate ();
			}
		});
	}

	void Update () {
		for (int i = 0; i < startButtons.Length; i++) {
			startButtons[i].rotation = Quaternion.Euler(Vector3.zero);
		}
	}

	public void CheckIfStartIsSet () {
		int count = 0;
		for (int i = 0; i < gears.Length; i++) {
			count += gears [i].isSet ? 1 : 0;
		}

		if(count >= gears.Length){
			StartisSet ();
		}

		mainCamera.transform.DOShakePosition (0.25f, 0.05f, 90);
	}

	void StartisSet () {
		LightItUp ();
	}

	void LightItUp () {
		float counter = 0.0f;
		DOTween.To(()=>counter , x => counter = x, 1.0f, 1.0f).OnUpdate(()=>{
			startBorder.fillAmount = counter;
		}).OnComplete(()=>{
			bulb.color = ColorPallette.instance.bulbActiveColor[0];
			bulb.fillAmount = 1.0f;
			for (int i = 0; i < titleTexts.Length; i++) {
				titleTexts [i].color = ColorPallette.instance.bulbActiveColor[0];;
			}
			lightParticle.SetActive (true);
			startText.SetActive (true);
		});
	}

	void ResetLight () {
		bulb.color = ColorPallette.instance.bulbInActiveColor[0];
		for (int i = 0; i < titleTexts.Length; i++) {
			titleTexts [i].color = ColorPallette.instance.bulbInActiveColor[0];
		}
		lightParticle.SetActive (false);
		startText.SetActive (false);
		particle_Blast.SetActive (false);
		startBorder.fillAmount = 0.0f;
		bigArrow.position = new Vector3 (0, -10, 90);
		mainCamera.transform.position = Vector3.zero;
		levelSelectGear.StopRotate ();
	}

	public void FireMainArrow () {
		bigArrow.DOMoveY (100, 5.0f);
		Yielder.instance.ExecuteAfterSeconds (0.25f, ()=>{
			bulb.color = ColorPallette.instance.bulbInActiveColor[0];
			particle_Blast.SetActive (true);
			lightParticle.SetActive (false);
			bulb.fillAmount = 0.0f;
			Yielder.instance.ExecuteAfterSeconds (1.0f, ()=>{
				mainCamera.transform.DOMoveY(10, 2.0f).OnComplete(()=>{
					levelSelectGear.Rotate ();
					CoinsManager.instance.SetGraphicsEnabled(true);
				});
			});
		});
	}

	void OnStageSwipe (int index) {
		if(LevelManager.instance != null){
			LevelManager.instance.StageChange (index);
		}

		//stageText.text = (index + 1).ToString ();
		SetStageColliders (index);
		SetStageColors (index);
	}

	public void SetStageColliders (int stageID) {
		if(stages.Count > 0){
			for (int i = 0; i < stages.Count; i++) {
				stages[i].myCollider.enabled = false;
			}

			int min = stageID * 10;
			int max = min + 9;
			for (int i = min; i <= max; i++) {
				if(stages[i].mUnlockStatus > 0){
					stages [i].myCollider.enabled = true;
				}
			}
		}
	}

	public void SetStageColors (int stageIndex) {
		shield.DOColor (ColorPallette.instance.cameraBGColor [stageIndex], 1.0f);
		mainCamera.DOColor (ColorPallette.instance.cameraBGColor [stageIndex], 1.0f);
		CoinsManager.instance.SetColor (stageIndex);
	}

	private void InitStages () {
		int numberOfStages = stagesParent.childCount;

		for (int i = 0; i < numberOfStages; i++) {
			Transform stage = stagesParent.GetChild (i);
			for (int j = 1; j < stage.childCount; j++) {
				LevelSelectionBulb bulb = stage.GetChild (j).GetComponent<LevelSelectionBulb> ();
				stages.Add (bulb);
				bulb.InitBulb (stages.Count, LevelManager.instance.GetLevelUnlockStatus (stages.Count + 1));
				bulb.Enable ();
			}
		}
	}
	 
	public void FireArrow (float speed = 1000.0f) {
		arrows [arrowIndex].Fire (speed, false);
		arrowIndex++;
	}

	public void ArrowHitSomething (Collider2D mCollider) {
		LevelSelectionBulb bulb = mCollider.transform.GetComponent<LevelSelectionBulb> ();
		if(bulb != null){
			if (CoinsManager.instance.HasEnoughCoins ()) {
				CoinsManager.instance.ChargeForLevel (100);
				bulb.TakeHit (mCollider);
				Yielder.instance.ExecuteAfterSeconds (2.0f, ()=>{
					LevelManager.instance.LoadScene ("MainGame", bulb.mLevelNumber);
				});
			}
			else {
				CoinsManager.instance.ShowAddMoreCoinsWindow();
			}
		}
	}


	public void ArrowHitNothing () {
		Debug.Log("Arrow hit: Nothing");
	}
}
