﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDataManager : MonoBehaviour {

	public static GameDataManager instance;
	public Arrow _pfArrow;
			
	public Transform usedArrowParent;
	public int currentLevelNumber = 3;

	private long _iCoins;

	public long Coins{
		get { return _iCoins; }
		set { _iCoins = value; }
	}

	[Header("Cheat Variables")]
	public bool usingCheats = false;
	public int cheatLevelNumber = 4;
	public int cheatStageNumberIndex = 0;

	void Awake () {
		instance = this;
	}

	public void SetCheats (int cLevelNumber, int cStageNumberIndex) {
		usingCheats = true;
		cheatLevelNumber = cLevelNumber;
		cheatStageNumberIndex = (cStageNumberIndex * 10) + 1;
	}

	public void ResetCheats () {
		usingCheats = false;
		cheatLevelNumber = 4;
		cheatStageNumberIndex = 0;
	}


}
