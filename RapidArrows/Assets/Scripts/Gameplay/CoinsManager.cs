﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CoinsManager : MonoBehaviour {

	public static CoinsManager instance;

	public RectTransform mainUICanvasTransform;
	public Text tCoinsText_Main; 
	public Text tCoinsText_Add; 
	public Image iBG;
	public Image iCoinGear;
	public ScoreBoard _ScoreBoard;

	void Awake () {
		instance = this;
	}

	void Start () {		
		SetAllGraphicsEnabled (false);
		InitializeCoins ();
		ShowCoins ();

	}

	public void SetGraphicsEnabled (bool bEnable) {
		float fAlpha = bEnable ? 1.0f : 0.0f;
		////tCoinsText_Main.color = new Color (tCoinsText_Main.color.r, tCoinsText_Main.color.g, tCoinsText_Main.color.b, fAlpha);
		_ScoreBoard.SetAlpha (fAlpha);
	}

	public float FadeGraphics (bool bFadeIn) {
		float fAlpha = bFadeIn ? 1.0f : 0.0f;
		float fDuration = 0.5f;
		////tCoinsText_Main.CrossFadeAlpha(fAlpha, fDuration, false);
		_ScoreBoard.FadeAlpha (fAlpha, fDuration);
		return fDuration;
	}

	public float SetColor (int iStageIndex) {
		float fDuration = 0.5f;
		////tCoinsText_Main.CrossFadeColor(ColorPallette.instance.bulbInActiveColor [iStageIndex], fDuration, false, false);
		_ScoreBoard.FadeColor (ColorPallette.instance.bulbActiveColor [iStageIndex], fDuration);
		iBG.CrossFadeColor(ColorPallette.instance.bulbActiveColor [iStageIndex], fDuration, false, false);
		iCoinGear.CrossFadeColor(ColorPallette.instance.bulbInActiveColor [iStageIndex], fDuration, false, false);
		return fDuration;
	}

	public void SetAllGraphicsEnabled (bool bEnable) {
		float fAlpha = bEnable ? 1.0f : 0.0f;
		////tCoinsText_Main.color = new Color (tCoinsText_Main.color.r, tCoinsText_Main.color.g, tCoinsText_Main.color.b, fAlpha);
		_ScoreBoard.SetAlpha (fAlpha);
		tCoinsText_Add.color = new Color (tCoinsText_Add.color.r, tCoinsText_Add.color.g, tCoinsText_Add.color.b, fAlpha);
	}

	public bool HasEnoughCoins () {
		return (GameDataManager.instance.Coins - 100) >= 0;
	}

	public void ShowAddMoreCoinsWindow () {
		
	}

	public void HideCoinsWindow () {
		
	}

	public float ShowCoins(){
		float fDuration = 1.0f;
		CountUp (GameDataManager.instance.Coins, fDuration);
		return fDuration;
	}

	public float AddCoins (long iCoinCount) {
		float fDuration = 1.0f;
		CountUp (GameDataManager.instance.Coins + iCoinCount, fDuration);
		fDuration += AnimateAddFont (iCoinCount, true);
		return fDuration;
	}

	public void ChargeForLevel (long iCoinCount) {
		GameDataManager.instance.Coins -= iCoinCount;
		CountUp (GameDataManager.instance.Coins, 1.0f);
		AnimateAddFont (iCoinCount, false);
	}

	private void InitializeCoins () {
		GameDataManager.instance.Coins += 1000;
		SetCoinsText (GameDataManager.instance.Coins);
	}

	private void SetCoinsText (long num) {
		////tCoinsText_Main.text = str;
		_ScoreBoard.SetNumber (num, false);
	}

	private Tween CountUp(long finalValue, float duration, Ease ease = Ease.Linear) {
		Tween tween = null;
			////Yielder.instance.CountUp (tCoinsText_Main, GameDataManager.instance.Coins, 
			////finalValue, duration, 0.0f, true, ease 
			////);
		_ScoreBoard.SetNumber (finalValue);

		return tween;
	}

	private float AnimateAddFont (long value, bool bAdd) {
		Color color;
		string mulitplier;
		if(bAdd)
		{
			color = ColorPallette.instance.bulbActiveColor [LevelManager.instance.GetStageIndexFromLevelID (GameDataManager.instance.currentLevelNumber)];
			mulitplier = "+";
		}
		else
		{
			color = ColorPallette.instance.red;
			mulitplier = "-";
		}

		float fDuration = 0.5f;

		tCoinsText_Add.text = mulitplier + value;
		tCoinsText_Add.color = color;
		tCoinsText_Add.CrossFadeAlpha(1.0f, fDuration, false);

		Yielder.instance.ExecuteAfterSeconds (fDuration*3, ()=>{
			tCoinsText_Add.CrossFadeAlpha(0.0f, fDuration, false);
		});

		return fDuration * 4;
	}

}
