﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

public class Bulb : MonoBehaviour {


	public bool isBroken = false;
	public int mLevelNumber = -1;
	public long iBulbWinAmount = 50;

	private Transform mTransform;
	private AudioSource audioSource;
	private Image mImage;
	private GameObject particle_Blast;
	private BoxCollider2D myCollider;

	void Awake () {
		//BulbManager.instance.RegisterBulb (this);
		mTransform = transform;
		myCollider = GetComponent<BoxCollider2D> ();
		audioSource = GetComponent<AudioSource> ();
		mImage = GetComponent<Image> ();
		particle_Blast = mTransform.Find ("Particle_Blast").gameObject;

		if(MainGame.instance.CurrentLevel != null){
			MainGame.instance.CurrentLevel.RegisterBulb (this);
		}
		else{
			Debug.LogError ("Current level is null");
		}
	}

	void OnEnable () {
		MainGame.instance.ArrowHitEvent += TakeHit;
	}

	void OnDisable () {
		MainGame.instance.ArrowHitEvent -= TakeHit;
	}

	public void TakeHit(Collider2D mCollider){
		//TODO: Inform Bulb manager

		if(mCollider.name == myCollider.name){
			mImage.enabled = false;
			audioSource.Play ();
			mCollider.enabled = false;
			isBroken = true;
			particle_Blast.SetActive (true);
			MainGame.instance.CurrentLevel.levelWinAmount += iBulbWinAmount;
		}
	}

	public void InitBulb (int levelID) {
		mLevelNumber = levelID;
		Disable ();
	}

	public void Enable () {
		myCollider.enabled = true;
//		mImage.color = ColorPallette.instance.bulbActiveColor [LevelManager.instance.GetStageIndexFromLevelID (mLevelNumber)];
		mImage.color = ColorPallette.instance.bulbActiveColor [LevelManager.instance.GetStageIndexFromLevelID (1)];//TODO
	}

	public void Disable () {
		ResetLevel ();
	}

	public void ResetLevel () {
		myCollider.enabled = false;
		mImage.color = ColorPallette.instance.bulbInActiveColor [LevelManager.instance.GetStageIndexFromLevelID (mLevelNumber)];
		particle_Blast.SetActive (false);
	}

	public void Show (float delay, float duration) {
		mTransform.localScale = Vector3.zero;
		Enable ();
		Yielder.instance.ExecuteAfterSeconds (delay, ()=>{
			mTransform.DOScale (Vector3.one, duration).SetEase(Ease.OutBack);
		});
	}
}
