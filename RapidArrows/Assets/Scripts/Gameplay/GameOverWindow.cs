﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameOverWindow : MonoBehaviour {

	public static GameOverWindow instance;

	public Transform gameOverWindow;
	public Image pauseBG;
	public Text text_Reward;
	public Text text_Collect;
	public Image[] buttonBGs;
	public Text reward;
	public ParticleSystem particles;
	public RectTransform mainCanvas;

	private bool mGameComplete = false;

	void Awake ()
	{
		instance = this;
	}

	void Start () 
	{
		Hide ();
	}

	public void Reward2X () 
	{
		if(mGameComplete)
		{
			//Reward 2X
			Collect (2);
		}
		else
		{
			//Watch add and continue
		}
	}

	public void Collect (int multiplier = 1) 
	{
		Vector3 worldPosition = Vector3.zero;
		RectTransformUtility.ScreenPointToWorldPointInRectangle (CoinsManager.instance.mainUICanvasTransform, 
			new Vector2(CoinsManager.instance._ScoreBoard.gear.position.x, CoinsManager.instance._ScoreBoard.gear.position.y), 
			Camera.main, out worldPosition);

		float fDuration = CoinsManager.instance.AddCoins (MainGame.instance.CurrentLevel.levelWinAmount * multiplier);

		particles.transform.position = reward.transform.position;//worldPosition;

		CountUp (reward, MainGame.instance.CurrentLevel.levelWinAmount, 0);

		Yielder.instance.ExecuteAfterSeconds ((fDuration / 10f), ()=>{
			particles.transform.DOMove (worldPosition, (fDuration / 4f)).OnComplete(()=>{
				particles.Stop(false);
			});
		});
		Yielder.instance.ExecuteAfterSeconds (fDuration, ()=>{
			Hide ();
			MainGame.instance.GoToHome();
		});
	}

	public void Show (bool bGameComplete) {
		ShowWindow (bGameComplete);
	}

	public void HideGameOverWindow () {
		Hide ();
	}

	private void ShowWindow (bool bGameComplete) 
	{
		mGameComplete = bGameComplete;
		string buttonText = bGameComplete ? "REWARD 2X" : "CONTINUE";
		text_Reward.text = buttonText;

		reward.color = ColorPallette.instance.bulbActiveColor [LevelManager.instance.GetStageIndexFromLevelID (GameDataManager.instance.currentLevelNumber)];
		//reward.text = "" + MainGame.instance.CurrentLevel.levelWinAmount;
		CountUp (reward, 0, MainGame.instance.CurrentLevel.levelWinAmount);

		gameOverWindow.gameObject.SetActive (true);

		for (int i = 0; i < buttonBGs.Length; i++) {
			buttonBGs[i].DOFade (1f, 0.25f);
			//			buttonBGs[i].transform.localScale = new Vector3(1f, 0.4f, 1f);
			Yielder.instance.DoScale (buttonBGs[i].transform, new Vector3(3f, 0.4f, 1f), 0.25f, 0.25f * (i + 5), Ease.OutExpo);
		}

		pauseBG.DOFade (70.0f / 255.0f, 0.25f);

		Yielder.instance.ExecuteAfterSeconds (1.0f, ()=>{
			text_Reward.DOFade (1.0f, 0.25f);
			text_Collect.DOFade (1.0f, 0.25f);
		});
	}

	private void Hide () 
	{
		gameOverWindow.gameObject.SetActive (false);

		for (int i = 0; i < buttonBGs.Length; i++) {
			buttonBGs[i].DOFade (0f, 0.0f);
			buttonBGs[i].transform.localScale = new Vector3(0f, 0.4f, 1f);
		}

		pauseBG.DOFade (0.0f, 0.0f);
		text_Reward.DOFade (0.0f, 0.0f);
		text_Collect.DOFade (0.0f, 0.0f);
	}

	private Tween CountUp (Text text, long startValue, long finalValue)
	{
		return Yielder.instance.CountUp (text, startValue, finalValue, 1.0f, 0.0f, false, Ease.Linear);
	}

}
