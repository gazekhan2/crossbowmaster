﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using DG.Tweening;

public class MainGame : MonoBehaviour {

	public static MainGame instance;

	/// public
	public delegate void pauseGame();
	public event pauseGame PauseGameEvent;
	public delegate void unPauseGame();
	public event pauseGame UnPauseGameEvent;
	public delegate void fireArrow();
	public event pauseGame FireArrowEvent;
	public delegate void ArrowHit(Collider2D mCollider);
	public event ArrowHit ArrowHitEvent;

	public Transform canvasTransform;
	public Camera mainCamera;

	/// private
	private bool _isGamePaused = false;
	private bool _isPlayable = false;
	private EventSystem _eventSystem;
	private Level _currentLevel = null;

	//***Temporary Variables
	public Text message;

	void Awake () {
		instance = this;
		GameDataManager.instance.usedArrowParent = canvasTransform;
	}

	void Start () {
		EventSystem = GameObject.Find("EventSystem").GetComponent<EventSystem>();

		SetupLevel ();
		//TODO: Use level load time instead of yielder
		Yielder.instance.ExecuteAfterSeconds (3.0f, (() => {
			//StartGame ();
		}));
	}

	void SetupLevel () {
		Color levelBGColor = ColorPallette.instance.cameraBGColor [LevelManager.instance.GetStageIndexFromLevelID (GameDataManager.instance.currentLevelNumber)];
		mainCamera.DOColor (levelBGColor, 1.0f).OnComplete(()=>{
			//BackgroundManager.instance.SetupLevelBackground ();
			LoadCurrentLevel();
		});
	}

	public void LoadCurrentLevel () {
		LevelManager.instance.LoadLevel (GameDataManager.instance.currentLevelNumber);
	}

	public void Fire () {
		if(IsPlayable)
			FireArrowEvent();
	}

	public void StartGame () {
		IsGamePaused = false;
		IsPlayable = true;
		CurrentLevel.Play();
	}

	public void PauseGame () {
		IsGamePaused = true;
		IsPlayable = false;
		PauseGameEvent();
		PauseWindow.instance.Show (true);
	}

	public void UnPaueGame () {
		IsGamePaused = false;
		IsPlayable = true;
		UnPauseGameEvent();
		PauseWindow.instance.Show (false);
	}

	public void GameOver (bool bGameComplete) {
		IsPlayable = false;
		print("GameOver");
		GameOverWindow.instance.Show (bGameComplete);
//		Yielder.instance.ExecuteAfterSeconds (2.0f, ()=>{
//			GameDataManager.instance.ResetCheats ();
//			LevelManager.instance.LoadScene ("Home");
//		});
	}

	public void GoToHome ()
	{
		GameDataManager.instance.ResetCheats ();
		LevelManager.instance.LoadScene ("Home");
	}

	public void ArrowHitSomething (Collider2D mCollider) {
		ArrowHitEvent (mCollider);
		CurrentLevel.CheckForGameComplete ();
	}

	public void ArrowHitNothing () {
		Debug.Log("Arrow hit: Nothing");
	}

	public void AddArrowsFromUsedOnes (int numberOfArrowsToAdd) {
		CurrentLevel.AddArrowsFromUsedOnes (numberOfArrowsToAdd);
	}

	public List<Arrow> AddNewArrows(int numberOfArrowsToAdd, bool makeAPartofLevel = false)
	{
		return CurrentLevel.AddNewArrows(numberOfArrowsToAdd, makeAPartofLevel);
	}

	#region POWER UP
	public void SetPowerup(int id)
	{
		PowerupManager.instance.SelectPowerup(id);
	}
    #endregion

    #region GetterSetters
    public bool IsGamePaused {
		get{ return _isGamePaused; }
		set{ _isGamePaused = value; }
	}

	public bool IsPlayable {
		get{ return _isPlayable; }
		set{ _isPlayable = value; }
	}

	public EventSystem EventSystem {
		get{ return _eventSystem; }
		set{ _eventSystem = value; }
	}

	public Level CurrentLevel {
		get{ return _currentLevel; }
		set{ _currentLevel = value; }
	}

	#endregion
}
