﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class LevelSelectionBulb : MonoBehaviour {


	private Transform mTransform;
	private AudioSource audioSource;
	private Image mImage;
	private Text mLevelID;
	private GameObject particle_Blast;

	[HideInInspector]
	public BoxCollider2D myCollider;

	public bool isBroken = false;
	public int mLevelNumber = -1;
	public int mUnlockStatus = - 1;

	void Start () {
		mTransform = transform;
		myCollider = GetComponent<BoxCollider2D> ();
		audioSource = GetComponent<AudioSource> ();
		mImage = GetComponent<Image> ();
		mLevelID = mTransform.Find ("LevelID").GetComponent<Text>();
		particle_Blast = mTransform.Find ("Particle_Blast").gameObject;
	}

	public void TakeHit(Collider2D mCollider){
		//TODO: Inform Bulb manager

		if(mCollider.name == myCollider.name){
			mImage.enabled = false;
			audioSource.Play ();
			mCollider.enabled = false;
			isBroken = true;
			particle_Blast.SetActive (true);
			//GameDataManager.instance.currentLevelNumber = mLevelNumber;
		}
	}


	public void InitBulb (int levelID, int unlockStatus) {
		mLevelNumber = levelID;
		mUnlockStatus = unlockStatus;
		mLevelID.text = mLevelNumber.ToString();
		Disable ();
	}

	public void Enable () {
		if(mUnlockStatus > 0){
			myCollider.enabled = true;
			mImage.color = ColorPallette.instance.bulbActiveColor [LevelManager.instance.GetStageIndexFromLevelID (mLevelNumber)];
		}
	}

	public void Disable () {
		ResetLevel ();
	}

	public void ResetLevel () {
		myCollider.enabled = false;
		mImage.color = ColorPallette.instance.bulbInActiveColor [LevelManager.instance.GetStageIndexFromLevelID (mLevelNumber)];
		particle_Blast.SetActive (false);
	}


}

