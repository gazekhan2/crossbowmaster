﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemainingArrows : MonoBehaviour {

	public static RemainingArrows instance;

	public Transform mRemainingArrowsTransform;
	public RemainingArrowsItem _pfRemainingArrowItem;

	private List<RemainingArrowsItem> listOfItems = new List<RemainingArrowsItem> ();

	void Awake (){
		instance = this;
	}

	public float Init () {
		float fAnimDuration = 0.4f;
		float fDelay = 0.2f;
		float fTotalDuration = fAnimDuration;

		int iNumberOfQuivers = MainGame.instance.CurrentLevel.listOfQuivers.Count;

		for (int i = 0; i < iNumberOfQuivers; i++) {
			RemainingArrowsItem item = Instantiate (_pfRemainingArrowItem, mRemainingArrowsTransform) as RemainingArrowsItem;
			listOfItems.Add (item);
			fDelay = i * 0.2f;
			item.Show (MainGame.instance.CurrentLevel.listOfQuivers [i].listOfRemainingArrows.Count.ToString (), fAnimDuration, fDelay);
			fTotalDuration += fDelay;
		}

		return fTotalDuration;
	}

	public void SetRemainingArrows (int iQuiverIndex, string text) {
		listOfItems [iQuiverIndex].SetText (text);
	}
}
