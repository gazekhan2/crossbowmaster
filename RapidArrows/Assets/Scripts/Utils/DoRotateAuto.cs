﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoRotateAuto : MonoBehaviour {

	public Transform mTransform;
	private Tween mTween;
	void Awake () {
		mTransform = this.transform;
	}

	void Start () {
		Rotate (360.0f, 5, -1, LoopType.Incremental, Ease.Linear);
	}

	void OnEnable(){
		MainGame.instance.PauseGameEvent += GamePause;
		MainGame.instance.UnPauseGameEvent += GameUnPause;
	}

	void OnDisable(){
		MainGame.instance.PauseGameEvent -= GamePause;
		MainGame.instance.UnPauseGameEvent += GameUnPause;
	}

	public void Rotate (float finalAngle, float speed, int numberOfLoops , LoopType loopType, Ease ease, System.Action action = null)
	{
		mTween = mTransform.DORotate (new Vector3 (0, 0, finalAngle), speed, RotateMode.FastBeyond360)
			.SetLoops (numberOfLoops, loopType).SetEase (ease).SetSpeedBased ().OnComplete (() => {
				if(action != null){
					action();
				}
			});
	}

	void GamePause(){
		mTween.Pause ();
	}

	void GameUnPause(){
		mTween.Play ();
	}

}
