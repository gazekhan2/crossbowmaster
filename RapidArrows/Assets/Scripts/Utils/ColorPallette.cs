﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorPallette : MonoBehaviour {

	public static ColorPallette instance;

	public Color [] cameraBGColor;
	public Color [] bulbActiveColor;
	public Color [] bulbInActiveColor;
	public Color [] bulbFontColor;
	public Color [] fontColor;
	public Color red;
	public Color green;


	void Awake () {
		instance = this;
	}
}
