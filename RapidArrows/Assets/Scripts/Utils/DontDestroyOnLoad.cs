﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class DontDestroyOnLoad : MonoBehaviour {

	// Use this for initialization
	void Awake () {
		DontDestroyOnLoad(this);
	}

	void Start () {
		SceneManager.LoadScene("Home");
	}
}
