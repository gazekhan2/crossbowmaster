﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class HomeScreenGearRotate : MonoBehaviour
{

	public int direction = 1;
	public DoRotate shadowGear;
	public DoRotate rod;
	public bool isSet = false;

	private float rodInitialRotation = 0.0f;
	private float mInitialRotation = 0.0f;
	private Vector2 rotationLimit;
	private float speed = 150.0f;
	private float finalAngle;
	private Transform mTransform;
	private bool isRotating = false;
	private DoRotate mRotate;

	void Awake () {
		mTransform = this.transform;		
		mRotate = GetComponent<DoRotate> ();
	}
	void Start ()
	{
		rodInitialRotation = rod.mTransform.eulerAngles.z;
		finalAngle = rodInitialRotation + 360;
		mInitialRotation = mTransform.eulerAngles.z;
		rotationLimit = new Vector2 (rodInitialRotation - 20.0f, rodInitialRotation + 20.0f);
		speed = Random.Range (speed - 10.0f, speed + 10.0f);
	}

	public void Rotate ()
	{
		//mButton.enabled = true;
		isRotating = true;
		mRotate.Rotate 		(direction * finalAngle, speed, 	-1, LoopType.Incremental, Ease.Linear);
		shadowGear.Rotate 	(direction * -1 * finalAngle, speed / 2, -1, LoopType.Incremental, Ease.Linear);
		rod.Rotate 			(direction * -1 * finalAngle, speed / 4, -1, LoopType.Incremental, Ease.Linear);
	}

	public void Clicked ()
	{
		if(!isSet){
			if(isRotating){
				if (IsWithinRange ()) {
					StopRotate ();
					isSet = true;
					mRotate.Rotate 		(direction * mInitialRotation, 	 	 speed, 	-1, LoopType.Incremental, Ease.Linear);
					shadowGear.Rotate 	(direction * -1 * 360,				 speed / 2,	-1, LoopType.Incremental, Ease.Linear);
					rod.Rotate 			(direction * -1 * rodInitialRotation, speed / 4, 0, LoopType.Incremental, Ease.Linear, ()=>{
						mRotate.StopRotate ();
						shadowGear.StopRotate ();
						HomeScreenManager.instance.CheckIfStartIsSet ();
					});
				}else{
					StopRotate ();
				}
			}else{
				Rotate ();
			}
		}
	}

	bool IsWithinRange ()
	{
		if (rod.mTransform.eulerAngles.z > rotationLimit.x && rod.mTransform.eulerAngles.z < rotationLimit.y) {
			return true;
		}
		return false;
	}

	public void StopRotate () {
		isRotating = false;
		mRotate.StopRotate ();
		shadowGear.StopRotate ();
		rod.StopRotate ();
	}
}
