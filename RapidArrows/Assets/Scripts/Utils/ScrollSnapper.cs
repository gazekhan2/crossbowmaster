﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ScrollSnapper : MonoBehaviour, IInitializePotentialDragHandler, IEndDragHandler {

	public Transform targetContent;
	public ScrollAxis scrollAxis = ScrollAxis.x;
	public float cellGroupSize = 1;
	public float cellWidth = 100;
	public Image[] bulletButtons;

	public delegate void snapEvent(int index);
	public event snapEvent SnappingEvent;

	Vector2 upPosition;
	Vector2 downPosition;
	float groupLenght;
	Vector3 finalPosition;

	void Start () {
		groupLenght = cellGroupSize * cellWidth;

		ChangeBulletColor(0);
	}

	bool tweenX = false;
	bool tweenY = false;

	void Update () {
		if(tweenX)
		{
			targetContent.localPosition = Vector3.Lerp(targetContent.localPosition, finalPosition, 0.25f);
			if(Mathf.Approximately(targetContent.localPosition.x, finalPosition.x))
			{
				targetContent.localPosition = finalPosition;
				tweenX = false;
			}
		}
		if(tweenY)
		{
			targetContent.localPosition = Vector3.Lerp(targetContent.localPosition, finalPosition, 0.25f);
			if(Mathf.Approximately(targetContent.localPosition.y, finalPosition.y))
			{
				targetContent.localPosition = finalPosition;
				tweenY = false;
			}
		}
	}

	public void OnEndDrag (PointerEventData data) {
		upPosition = data.position;

		float x = scrollAxis == ScrollAxis.x ? 
								downPosition.x - upPosition.x: 
								downPosition.y - upPosition.y;

		if(x < 0)
			Snap (1);
		else if(x > 0)
			Snap(-1);

	}
	public void OnInitializePotentialDrag (PointerEventData data) {
		downPosition = data.position;
	}

	public void Snap (int direction) {
		float delta = groupLenght;
		float position = 0.0f;
		float targetPosition = scrollAxis == ScrollAxis.x ? 
											targetContent.localPosition.x : 
											targetContent.localPosition.y;

		float MOD = Mathf.Abs(targetPosition % groupLenght);

		if(Mathf.Abs(MOD) > 0) {
			if(direction < 0) {
				if(scrollAxis == ScrollAxis.x)
					delta = groupLenght - MOD;
				else
					delta = MOD;
			}
			else if(direction > 0) {
				if(scrollAxis == ScrollAxis.x)
					delta = MOD;
				else
					delta = groupLenght - MOD;
			}
		}

		position = Mathf.RoundToInt((delta * direction) + targetPosition);

		if(Mathf.Abs(position) > ((targetContent.childCount * cellWidth) - groupLenght))
		{
			position = ((targetContent.childCount * cellWidth) - groupLenght) * Mathf.Sign(position);
		}

		if(scrollAxis == ScrollAxis.x && -position >= 0)
		{
			finalPosition = new Vector3(position, targetContent.localPosition.y, targetContent.localPosition.z);
			tweenX = true;
		}
		if(scrollAxis == ScrollAxis.y && position >= 0)
		{
			finalPosition = new Vector3(targetContent.localPosition.x, position, targetContent.localPosition.z);
			tweenY = true;
		}

		int index = Mathf.Abs((int)(position / groupLenght));
		ChangeBulletColor(index);
	}

	public void SnapTo (float position) {
		if(scrollAxis == ScrollAxis.x)
		{
			finalPosition = new Vector3(position, targetContent.localPosition.y, targetContent.localPosition.z);
			tweenX = true;
		}
			
		else
		{
			finalPosition = new Vector3(targetContent.localPosition.x, position, targetContent.localPosition.z);
			tweenY = true;
		}
		
	}

	public void ChangeBulletColor (int index) {
		SnappingEvent (index);
		if (bulletButtons.Length <= 0)
			return;
		
		Image bullet = bulletButtons[index];
		for (int i = 0; i < bulletButtons.Length; i++) {
			bulletButtons[i].color = new Color(bulletButtons[i].color.r, bulletButtons[i].color.g, bulletButtons[i].color.b, 0.2f);
		}
		if(bullet != null)
			bullet.color = new Color(bullet.color.r, bullet.color.g, bullet.color.b, 1.0f);
	}

}

public enum ScrollAxis {
	x,
	y
}