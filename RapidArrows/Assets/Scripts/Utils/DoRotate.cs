﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoRotate : MonoBehaviour {

	[HideInInspector]
	public Transform mTransform;
	private Tween mTween;
	void Awake () {
		mTransform = this.transform;
	}

	public void Rotate (float finalAngle, float speed, int numberOfLoops , LoopType loopType, Ease ease, System.Action action = null)
	{
		mTween = mTransform.DORotate (new Vector3 (0, 0, finalAngle), speed, RotateMode.FastBeyond360)
			.SetLoops (numberOfLoops, loopType).SetEase (ease).SetSpeedBased ().OnComplete (() => {
				if(action != null){
					action();
				}
			});
	}

	public void StopRotate(){
		mTween.Kill ();
		mTween = null;
	}

}
