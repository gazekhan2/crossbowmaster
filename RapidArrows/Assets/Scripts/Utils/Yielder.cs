﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class Yielder : MonoBehaviour {

	public static Yielder instance;

	void Awake (){
		instance = this;
	}

	public Tween ExecuteAfterSeconds(float time, System.Action onCompleteAction = null){
		float counter = 0.0f;
		Tween tween = DOTween.To(()=>counter , x => counter = x, 1.0f, time).OnComplete(()=>{
			if(onCompleteAction != null)
				onCompleteAction();
		});

		return tween;
	}

	public Tween DoScale(Transform mtransform, Vector3 finalScale, float duration, float delay, Ease ease, System.Action onCompleteAction = null){
		Tween tween = null;
		ExecuteAfterSeconds (delay, ()=>{
			tween = mtransform.DOScale (finalScale, duration).SetEase (ease).OnComplete(()=>{
				if(onCompleteAction != null)
					onCompleteAction();
			});
		});

		return tween;
	}

	public Tween CountUp(int startValue, int finalValue, float duration, float delay, Ease ease, System.Action onCompleteAction = null) {
		Tween tween = null;
		int iCounter = startValue;
		ExecuteAfterSeconds (delay, ()=>{
			tween = DOTween.To(()=>iCounter, x=> iCounter = x, finalValue, duration).SetEase (ease).OnComplete(()=>{
				if(onCompleteAction != null)
					onCompleteAction();
			});
		});

		return tween;
	}

	public Tween CountUp(Text text, long startValue, long finalValue, float duration,float delay, bool addFinalValueToCoins, Ease ease = Ease.Linear, System.Action onUpdateAction = null, System.Action onCompleteAction = null) {
		Tween tween = null;
		long iCounter = startValue;

		tween = DOTween.To (() => iCounter, x => iCounter = x, finalValue, duration).SetEase (ease)
			.OnUpdate (() => {
				text.text = "" + iCounter;
			})
			.OnComplete (() => {
				if(onCompleteAction != null)
					onCompleteAction();
				if(addFinalValueToCoins)
					GameDataManager.instance.Coins = iCounter;
			});

		return tween;
	}
}
